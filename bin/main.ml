let path = Sys.argv.(1)

let rec iter_over_file ic f =
  match In_channel.input_line ic with
  | None -> ()
  | Some s ->
      f s;
      iter_over_file ic f

module Hashtbl = Base.Hashtbl

type stats = {
  mutable sum : float;
  mutable nb : float;
  mutable maximum : float;
  mutable minimum : float;
}

let pp_stats s =
  Format.sprintf "%.1f/%.1f/%.1f" s.minimum (s.sum /. s.nb) s.maximum

let _ =
  let dict = Core.String.Table.create () ~size:100 in
      In_channel.with_open_text path (fun x ->
      iter_over_file x (fun s ->
          match String.split_on_char ';' s with
          | [ name; temp ] -> (
              let temp = float_of_string temp in
              match Hashtbl.find dict name with
              | None ->
                  ignore @@ Hashtbl.add dict ~key:name
                      ~data:
                        { sum = temp; maximum = temp; minimum = temp; nb = 1. }
              | Some prev ->
                  prev.nb <- prev.nb +. 1.;
                  prev.sum <- prev.sum +. temp;
                  if temp > prev.maximum then prev.maximum <- temp;
                  if temp < prev.minimum then prev.minimum <- temp;
              )
          | _ -> failwith "incorect line"
          );
      );

      dict
        |> Hashtbl.to_alist
        |> List.sort (fun (s1, _) (s2, _)-> String.compare s1 s2)
        |> List.map (fun (s, stat) -> s ^ "=" ^ (pp_stats stat))
        |> List.fold_left (fun acc x -> acc ^ ", " ^ x) ""
        |> fun s -> "{" ^ s ^ "}"
        |> Format.print_string
